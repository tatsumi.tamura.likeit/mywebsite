package model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

public class ItemBean implements Serializable {

	private int id;
	private int exhibitUserId;
	private String itemName;
	private String itemDetailText;
	private String fileName;
	private int price;
	private int deleteTag;

	private String statusName;
	private String categoryName;

	private String sellerName;
	private String sellerEvaluation;

	public ItemBean(int itemId, String itemName, String itemDetailText, String fileName, int price, String statusName,
			String categoryName) {

		this.id = itemId;
		this.itemName = itemName;
		this.itemDetailText = itemDetailText;
		this.fileName = fileName;
		this.price = price;
		this.statusName = statusName;
		this.categoryName = categoryName;

	}

	public ItemBean() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getExhibitUserId() {
		return exhibitUserId;
	}

	public void setExhibitUserId(int exhibitUserId) {
		this.exhibitUserId = exhibitUserId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDetailText() {
		return itemDetailText;
	}

	public void setItemDetailText(String itemDetailText) {
		this.itemDetailText = itemDetailText;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getDeleteTag() {
		return deleteTag;
	}

	public void setDeleteTag(int deleteTag) {
		this.deleteTag = deleteTag;
	}

}
