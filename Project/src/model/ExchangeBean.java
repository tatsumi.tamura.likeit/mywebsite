package model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

public class ExchangeBean implements Serializable {

	private int id;
	private int exchangeUserId;
	private int exchangeBuyId;
	private String exchangeText;
	private String createDate;

}
