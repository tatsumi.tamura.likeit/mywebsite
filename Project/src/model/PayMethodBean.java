package model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

public class PayMethodBean implements Serializable {

	private int id;
	private String payMethodName;
	private int payMethodPrice;
	private String createDate;

}
