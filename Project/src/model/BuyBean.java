package model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

public class BuyBean implements Serializable {

	private int id;
	private int buyUserId;
	private int itemId;
	private int totalPrice;
	private int payMethodId;
	private boolean status;
	private String buyDate;

}
