package model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

public class CommentBean implements Serializable {

	private int id;
	private int commentUserId;
	private int CommentItemName;
	private String commentText;
	private String createDate;

}
