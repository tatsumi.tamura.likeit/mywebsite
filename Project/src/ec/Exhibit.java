package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.ItemDao;
import dao.UserDao;
import model.ItemBean;
import model.UserBean;

/**
 * Servlet implementation class Insert_item
 */
@WebServlet("/Exhibit")
@MultipartConfig(location = "/Users/tatsumi/Documents/mywebsite/Project/WebContent/upload", maxFileSize = 10485760)
public class Exhibit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Exhibit() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/exhibit.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		int userId = ((UserBean) session.getAttribute("userInfo")).getId();
		String itemName = request.getParameter("item_name");
		String itemText = request.getParameter("item_text");
		String itemStatus = request.getParameter("item_status");
		Part itemImage = request.getPart("item_image");
		String itemCategory = request.getParameter("item_category");
		String itemPrice = request.getParameter("item_price");
		String deleteTag = "1";

		String name = this.getFileName(itemImage);
		itemImage.write(name);

		ItemDao itemDao = new ItemDao();
		itemDao.exhibitItem(userId, itemName, itemText, name, itemStatus, itemCategory, itemPrice, deleteTag);

		response.sendRedirect("ItemReference");
	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
