package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.UserDao;
import model.UserBean;

/**
 * Servlet implementation class UserInsert
 */
@WebServlet("/UserInsert")
@MultipartConfig(location = "/Users/tatsumi/mywebsite/Project/WebContent/upload", maxFileSize = 10485760)
public class UserInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserInsert() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_insert.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("login_password");
		String rePassword = request.getParameter("login_repassword");
		String loginName = request.getParameter("login_username");
		Part iconImage = request.getPart("login_icon_image");
		String introduceText = request.getParameter("login_introduce_text");
		String address = request.getParameter("login_address");

		String name = this.getFileName(iconImage);
		iconImage.write(name);

		EcHelper eh = new EcHelper();
		String source = eh.secret(password);

		UserBean ub = new UserBean();
		ub.setLoginId(loginId);
		ub.setPassword(source);
		ub.setName(loginName);
		ub.setFileName(name);
		ub.setIntroduceText(introduceText);
		ub.setAddress(address);

		UserDao userDao = new UserDao();
		userDao.addUser(ub);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
