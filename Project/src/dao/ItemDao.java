package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Part;
import model.ItemBean;

public class ItemDao {

	// ---------------------------------一覧表示---------------------------------//

	public List<ItemBean> showAll() {

		Connection conn = null;
		List<ItemBean> itemList = new ArrayList<ItemBean>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item INNER JOIN status ON item.status = status.status_id "
					+ "INNER JOIN category ON item.category = category.category_id";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {

				int itemId = rs.getInt("id");
				String itemName = rs.getString("item_name");
				String itemDetailText = rs.getString("item_detail_text");
				String fileName = rs.getString("item_file_name");
				int price = rs.getInt("price");
				String statusName = rs.getString("status_name");
				String categoryName = rs.getString("category_name");

				ItemBean item = new ItemBean(itemId, itemName, itemDetailText, fileName, price, statusName,
						categoryName);
				itemList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return itemList;

	}

	// ---------------------------------詳細表示---------------------------------//

	public ItemBean showReference(String id) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item INNER JOIN status ON item.status = status.status_id "
					+ "INNER JOIN category ON item.category = category.category_id WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int itemId = rs.getInt("id");
			String itemName = rs.getString("item_name");
			String itemDetailText = rs.getString("item_detail_text");
			String fileName = rs.getString("item_file_name");
			int price = rs.getInt("price");
			String statusName = rs.getString("status_name");
			String categoryName = rs.getString("category_name");

			ps.executeQuery();

			ItemBean item = new ItemBean(itemId, itemName, itemDetailText, fileName, price, statusName, categoryName);

			return item;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	// ---------------------------------出品---------------------------------//

	public ItemBean exhibitItem(int userId, String itemName, String itemText, String name, String itemStatus,
			String itemCategory, String itemPrice, String deleteTag) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO item (exhibit_user_id,item_name,item_detail_text,item_file_name,"
					+ "category,price,status,delete_tag)VALUES(?,?,?,?,?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, userId);
			ps.setString(2, itemName);
			ps.setString(3, itemText);
			ps.setString(4, name);
			ps.setString(5, itemCategory);
			ps.setString(6, itemPrice);
			ps.setString(7, itemStatus);
			ps.setString(8, deleteTag);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
