<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>

	<div class="container">
		<nav
			class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark p-5 mb-5">
			<a href="Top" class="back-button">くく</a>
			<p class="top_text">商品の編集</p>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navmenu1" aria-controls="navmenu1"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end"
				id="navmenu1">
				<div class="navbar-nav">
					<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
						class="nav-item2 nav-link" href="Exhibit">出品</a> <a
						class="nav-item3 nav-link" href="Logout">ログアウト</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="container-edit">

		<div class="box_mypage">
			<form action="item_reference.html" method="post">
				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-name-exhivit">商品名</h4>
					</div>

					<div class="col-sm-8">
						<input type="text" class="item-name-exhivit-text">
					</div>

				</div>

				<hr class=hr_exhibit>

				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-explanation-exhivit">説明文</h4>
					</div>

					<div class="col-sm-8">
						<textarea rows="5" cols="30" class="item-explanation-exhivit-text"></textarea>
					</div>

				</div>

				<hr class=hr_exhibit>

				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-status-exhivit">商品の状態</h4>
					</div>

					<div class="col-sm-4">
						<select name="status"
							class="item-status-exhivit-select form-control">
							<option value="新品" name="new">新品</option>
							<option value="良い" name="good">良い</option>
							<option value="悪い" name="bad">悪い</option>
						</select>

					</div>

				</div>

				<hr class=hr_exhibit>


				<h4 class="item-image-exhivit">商品画像</h4>
				<div class="row">

					<div class="col-sm-12">
						<img class="item-image-exhivit-image"
							src="https://placehold.jp/140x140.png" alt="カードの画像">
					</div>

					<div class="col-sm-12">
						<input type="file"
							class="item-image-exhivit-file form-control-file">
					</div>

				</div>

				<hr class=hr_exhibit>

				<div class="row">
					<div class="col-sm-6">
						<p class="category-exhibit-text">カテゴリー</p>
					</div>
					<div class="col-sm-4">
						<select name="category" class="category-exhibit form-control">
							<option value="衣類" name="cloth">衣類</option>
							<option value="本" name="book">本</option>
							<option value="食品" name="food">食品</option>
							<option value="ゲーム" name="game">ゲーム</option>
							<option value="雑貨" name="staff">雑貨</option>
							<option value="美容" name="cosmetic">美容</option>
							<option value="家電" name="appliance">家電</option>
						</select>
					</div>
				</div>

				<hr class=hr_exhibit>

				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-value-exhivit">販売価格</h4>
					</div>

					<div class="col-sm-8">
						<input type="text" class="item-value-exhivit-text">円
					</div>

				</div>

				<hr class=hr_exhibit>

				<input type="submit"
					class="btn btn-exhivit btn-success stretched-link" value="編集"
					id="send">

			</form>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>
</body>
</html>