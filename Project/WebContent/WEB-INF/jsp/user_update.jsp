<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>


</head>
<body>

	<div class="container">


		<nav
			class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark p-5 mb-5">
			<a href="Top" class="back-button">くく</a>
			<p class="top_text">ユーザ情報更新</p>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navmenu1" aria-controls="navmenu1"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end"
				id="navmenu1">
				<div class="navbar-nav">
					<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
						class="nav-item2 nav-link" href="Exhibit">出品</a> <a
						class="nav-item3 nav-link" href="Logout">ログアウト</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="container-insert">

		<!-- 		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if> -->

		<div class="box_update">
			<form action="mypage.html" method="post">

				<div class="row">
					<div class="col-sm-2">
						<p class="insert_login_text">ログインID</p>
					</div>
					<div class="col-sm-9">
						<input type="text" size="48" name="login_id" class="insert_login">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p class="insert_password_text">パスワード</p>
					</div>
					<div class="col-sm-9">
						<input type="password" size="48" name="login_password"
							class="insert_password">
					</div>
				</div>

				<div class="repassword">
					<div class="row">
						<div class="col-sm-4">
							<p class="insert_repassword_text">パスワード(確認)</p>
						</div>
						<div class="col-sm-8">
							<input type="password" size="20" name="login_repassword"
								class="insert_repassword">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p class="insert_user_text">ユーザ名</p>
					</div>
					<div class="col-sm-9">
						<input type="text" size="48" name="login_username"
							class="insert_user">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p class="insert_user_icon">アイコン画像</p>
					</div>
					<div class="col-sm-9">
						<input type="file" class="icon_image form-control-file">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p class="introduce_text">自己紹介文</p>
					</div>
					<div class="col-sm-9">
						<textarea rows="5" cols="30" class="introduce"></textarea>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p class="address_text">住所</p>
					</div>

					<p class="address_number_text">郵便番号(7桁)</p>

					<div class="col-sm-9">
						〒<input type="text" name="zip11" size="10" maxlength="8"
							class="address_number"
							onKeyUp="AjaxZip3.zip2addr(this,'','addr11','addr11');">
					</div>
				</div>

				<p class="address_auto_text">住所(都道府県+以降の住所)</p>

				<div>
					<input type="text" name="addr11" size="60" class="address_auto">
				</div>

				<input type="submit" class="btn btn-success stretched-link"
					value="更新" id="update">
			</form>
		</div>

	</div>



	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>
</body>
</html>