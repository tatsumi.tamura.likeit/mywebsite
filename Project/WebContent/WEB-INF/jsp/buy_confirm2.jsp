<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>


	<nav class="navbar navbar-expand-sm navbar-dark bg-dark p-5">
		<a href="Top" class="back-button">くく</a>
		<p class="top_text">購入手続き</p>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navmenu1" aria-controls="navmenu1"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end"
			id="navmenu1">
			<div class="navbar-nav">
				<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
					class="nav-item2 nav-link" href="Exhibit">出品</a> <a
					class="nav-item3 nav-link" href="Logout">ログアウト</a>
			</div>
		</div>
	</nav>

	<div class="box_confirm">

		<div class="row">

			<div class="col-sm-6">
				<img class="item-img-confirm" src="https://placehold.jp/150x150.png"
					alt="カードの画像">
			</div>

			<div class="col-sm-6">
				<h2 class="confirm-item-name">商品名(例1)</h2>
			</div>

			<div class="col-sm-12">
				<p class="confirm-item-price">${item.price}円</p>
			</div>

		</div>

		<hr class=hr_confirm>

		<form action="exchange_c.html" method="post">
			<div class="row">
				<div class="col-sm-6">
					<p class="use-point-text">使用するポイント</p>
				</div>
				<div class="col-sm-6">
					<p class="use-point">${user.point}ポイント
				</div>

				<div class="col-sm-6">
					<p class="own-point">所持ポイント : ${user.point}</p>
				</div>

				<div class="col-sm-6">
					<p class="accumulate-point">今回のご購入で〇〇ポイント貯まります</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<p class="pay-method-text">支払い方法</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-5">
					<p class="pay-method-confirm2">${pay.method}</p>
				</div>
				<div class="pay_price_whole">
					<div class="col-sm-5">
						<p class="pay-price-confirm2">${pay.price}円</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<p class="pay-price-text">支払い金額</p>
				</div>
				<div class="col-sm-6">
					<p class="pay-price">${totalPrice}円</p>
				</div>
			</div>

			<input type="submit" class="confirm-buy-button btn-primary"
				value="購入" id="send">
		</form>
	</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>
</body>
</html>