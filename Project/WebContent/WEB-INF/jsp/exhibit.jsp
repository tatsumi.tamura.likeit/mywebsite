<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>

	<div class="container">
		<nav
			class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark p-5 mb-5">
			<a href="Top" class="back-button">くく</a>
			<p class="top_text">出品</p>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navmenu1" aria-controls="navmenu1"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end"
				id="navmenu1">
				<div class="navbar-nav">
					<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
						class="nav-item2 nav-link" href="Exhibit">出品</a> <a
						class="nav-item3 nav-link" href="Logout">ログアウト</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="container-exhibit">

		<div class="box_mypage">
			<form action="Exhibit" method="post" enctype="multipart/form-data">
				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-name-exhivit">商品名</h4>
					</div>

					<div class="col-sm-8">
						<input type="text" class="item-name-exhivit-text" name="item_name">
					</div>

				</div>

				<hr class=hr_exhibit>

				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-explanation-exhivit">説明文</h4>
					</div>

					<div class="col-sm-8">
						<textarea rows="5" cols="30" class="item-explanation-exhivit-text"
							name="item_text"></textarea>
					</div>

				</div>

				<hr class=hr_exhibit>

				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-status-exhivit">商品の状態</h4>
					</div>

					<div class="col-sm-4">
						<select class="item-status-exhivit-select form-control"
							name="item_status">
							<option value="1" name="1">新品</option>
							<option value="2" name="2">良い</option>
							<option value="3" name="3">悪い</option>
						</select>

					</div>

				</div>

				<hr class=hr_exhibit>


				<h4 class="item-image-exhivit">商品画像</h4>
				<div class="row">

					<div class="col-sm-12">
						<img class="item-image-exhivit-image"
							src="https://placehold.jp/140x140.png" alt="カードの画像">
					</div>

					<div class="col-sm-12">
						<input type="file"
							class="item-image-exhivit-file form-control-file"
							name="item_image">
					</div>

				</div>

				<hr class=hr_exhibit>

				<div class="row">
					<div class="col-sm-6">
						<p class="category-exhibit-text">カテゴリー</p>
					</div>
					<div class="col-sm-4">
						<select class="category-exhibit form-control" name="item_category">
							<option value="1" name="1">衣類</option>
							<option value="2" name="2">本</option>
							<option value="3" name="3">食品</option>
							<option value="4" name="4">ゲーム</option>
							<option value="5" name="5">雑貨</option>
							<option value="6" name="6">美容</option>
							<option value="7" name="7">家電</option>
							<option value="8" name="8">その他</option>
						</select>
					</div>
				</div>

				<hr class=hr_exhibit>

				<div class="row">

					<div class="col-sm-4">
						<h4 class="item-value-exhivit">販売価格</h4>
					</div>

					<div class="col-sm-8">
						<input type="text" class="item-value-exhivit-text"
							name="item_price">円
					</div>

				</div>

				<hr class=hr_exhibit>

				<input type="submit" class="btn-exhivit btn-primary stretched-link"
					value="出品" id="send">

			</form>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>
</body>
</html>