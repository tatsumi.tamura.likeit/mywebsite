<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>

	<div class="container">
		<nav
			class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark p-5 mb-5">
			<a href="Top" class="back-button">くく</a>
			<p class="top_text">マイページ</p>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navmenu1" aria-controls="navmenu1"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end"
				id="navmenu1">
				<div class="navbar-nav">
					<a class="nav-item1 nav-link" href="BuyHistory">購入履歴</a> <a
						class="nav-item2 nav-link" href="Inquiry">問い合わせ</a> <a
						class="nav-item3 nav-link" href="Login">ログアウト</a>
				</div>
			</div>
		</nav>
	</div>

	<h1 class="mypage_h1">マイページ</h1>

	<div class="container-mypage">

		<div class="box_mypage">

			<div class="row">
				<div class="col-sm-12">
					<a href="user_update.html" class="user-update-link">ユーザ情報更新</a>
				</div>

				<div class="col-sm-6">
					<img class="mypage-image" src="https://placehold.jp/125x125.png"
						alt="カードの画像">
				</div>
				<div class="col-sm-6">
					<p class="username_text2">${user.name}</p>
				</div>
			</div>

			<hr class=hr_mypage>

			<div class="row">
				<div class="col-sm-2">
					<p class="evaluation_text1">評価</p>
				</div>

				<div class="col-sm-2">
					<p class="star1">★</p>
				</div>

				<div class="col-sm-2">
					<p class="star2">★</p>
				</div>

				<div class="col-sm-2">
					<p class="star3">★</p>
				</div>

				<div class="col-sm-3">
					<p class="evaluation_count">
						<a href="evaluation_list.html">(110)</a>
					</p>
				</div>

			</div>
			<hr class=hr_mypage>

			<p class="introduce_mypage_text">
				〇〇と申します。<br>始めたばかりですが、気持ちの良い取引をしていきたいです。<br>
				どうぞよろしくお願いします！
			</p>

			<hr class=hr_mypage>

			<div class="row">
				<div class="col-sm-6">
					<p class="point_text1">現在のポイント数</p>
				</div>

				<div class="col-sm-6">
					<p class="point_text2">${user.point}</p>
				</div>
			</div>

			<hr class=hr_mypage>

			<div class="row">

				<div class="col-sm-3">
					<a href="item_reference.html"
						class="card border-secondary
					mb-3" style="width: 12rem;">
						<img class="card-img-top" src="https://placehold.jp/100x100.png"
						alt="カードの画像">
						<div class="card-body">
							<h5 class="card-title">商品サンプル１</h5>
						</div>
					</a>
				</div>

				<div class="col-sm-3">
					<a href="item_reference.html"
						class="card border-secondary
					mb-3" style="width: 12rem;">
						<img class="card-img-top" src="https://placehold.jp/100x100.png"
						alt="カードの画像">
						<div class="card-body">
							<h5 class="card-title">商品サンプル２</h5>
						</div>
					</a>
				</div>

				<div class="col-sm-3">
					<a href="item_reference.html"
						class="card border-secondary
					mb-3" style="width: 12rem;">
						<img class="card-img-top" src="https://placehold.jp/100x100.png"
						alt="カードの画像">
						<div class="card-body">
							<h5 class="card-title">商品サンプル３</h5>
						</div>
					</a>
				</div>

			</div>
		</div>


	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>
</body>
</html>