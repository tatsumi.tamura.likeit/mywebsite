<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>

	<div class="container">
		<nav
			class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark p-5 mb-5">
			<a href="Top" class="back-button">くく</a>
			<p class="top_text">問い合わせ</p>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navmenu1" aria-controls="navmenu1"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end"
				id="navmenu1">
				<div class="navbar-nav">
					<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
						class="nav-item2 nav-link" href="Exhibit">出品</a> <a
						class="nav-item3 nav-link" href="Logout">ログアウト</a>
				</div>
			</div>
		</nav>
	</div>

	<h1 class="common_h1">出品</h1>

	<div class="container-inquiry-result">

		<h2 class="inquiry-result-text">問い合わせが送信されました。</h2>

		<a href="top.html">
			<button type="button" class="btn-inquiry-result btn-primary">TOPページへ</button>
		</a>

	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>
</body>
</html>