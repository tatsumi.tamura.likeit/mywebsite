<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>

	<nav
		class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark p-5 mb-5">
		<a href="Top" class="back-button">くく</a>
		<p class="top_text">評価一覧</p>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navmenu1" aria-controls="navmenu1"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end"
			id="navmenu1">
			<div class="navbar-nav">
				<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
					class="nav-item2 nav-link" href="Exhibit">出品</a> <a
					class="nav-item3 nav-link" href="Logout">ログアウト</a>
			</div>
		</div>
	</nav>

	<div class="evaluation-list">
		<ul
			class="nav nav-tabs nav-expand-lg nav-light bg-light w-100 nav-justified">
			<li class="nav-item"><a href="evaluation_list.html"
				class="nav-link">すべて</a></li>
			<li class="nav-item"><a href="evaluation_1.html"
				class="nav-link">★</a></li>
			<li class="nav-item"><a href="evaluation_2.html"
				class="nav-link">★★</a></li>
			<li class="nav-item"><a href="evaluation_3.html"
				class="nav-link active">★★★</a></li>
		</ul>

		<div class="row">
			<div class="col-sm-4">
				<img class="evaluation-image" src="https://placehold.jp/125x125.png"
					alt="カードの画像">
			</div>
			<div class="col-sm-8">
				<p class="evaluation-text">
					迅速かつ丁寧なご対応でした。<br>この度は誠に有難うございました。
				</p>
			</div>

			<div class="col-sm-12">
				<hr class="evaluation-hr">
			</div>

		</div>

		<div class="row">
			<div class="col-sm-4">
				<img class="evaluation-image" src="https://placehold.jp/125x125.png"
					alt="カードの画像">
			</div>

			<div class="col-sm-8">
				<p class="evaluation-text">美品と書いてあったのに、商品がすごく汚れていました。</p>
			</div>

			<div class="col-sm-12">
				<hr class="evaluation-hr">
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<img class="evaluation-image" src="https://placehold.jp/125x125.png"
					alt="カードの画像">
			</div>

			<div class="col-sm-8">
				<p class="evaluation-text">商品が汚れまみれでした。</p>
			</div>

			<div class="col-sm-12">
				<hr class="evaluation-hr">
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">© 1995-2019, Anyone.com, Inc.</p>
		</div>
	</footer>


</body>
</html>