<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>


	<nav class="navbar navbar-expand-sm navbar-dark bg-dark p-5">
		<a href="top.html" class="back-button">くく</a>
		<p class="Top">商品詳細</p>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navmenu1" aria-controls="navmenu1"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end"
			id="navmenu1">
			<div class="navbar-nav">
				<a class="nav-item1 nav-link" href="MyPage">マイページ</a> <a
					class="nav-item2 nav-link" href="Exhibit">出品</a> <a
					class="nav-item3 nav-link" href="Logout">ログアウト</a>
			</div>
		</div>
	</nav>

	<div class="box_reference">
		<div class="whole">

			<a href="ItemEdit" target="_blank">
				<button type="button" class="btn reference-edit btn-success">編集</button>
			</a> <a href="Delete" target="_blank">
				<button type="button" class="btn reference-delete btn-danger">消去</button>
			</a>

			<!-- 		<div class="vartical_line-left"
			style="background-color: gray; width: 1px; height: 40px;"></div>

		<div class="vartical_line-right"
			style="background-color: gray; width: 1px; height: 40px;"></div> -->

			<p class="item-category-small">${item.categoryName}</p>

			<div class="row">
				<img class="card-img-item-reference"
					src="https://placehold.jp/250x250.png" alt="カードの画像">

				<div class="container-item-reference">

					<div class="col-sm-12">
						<h1 class="item-name">${item.itemName}</h1>
					</div>

					<div class="col-sm-6">
						<a href="Favorite" target="_blank">
							<button type="button" class="item-favorite btn-primary">お気に入り追加</button>
						</a>
					</div>

					<div class="col-sm-3">
						<a href="#" target="_blank">
							<button type="button" class="item-facebook btn-primary">F</button>
						</a>
					</div>

					<div class="col-sm-3">
						<a href="#" target="_blank">
							<button type="button" class="item-twitter btn-primary">T</button>
						</a>
					</div>

				</div>

				<hr class=hr_reference>

				<div class="row">
					<div class="col-sm-9">
						<p class="item-sentence">
							${item.itemDetailText} <br>
						</p>
					</div>
				</div>

				<hr class=hr_reference>

				<div class="row">
					<div class="col-sm-6">
						<p class="category-small-text">カテゴリー</p>
					</div>
					<div class="col-sm-6">
						<p class="category-small">${item.categoryName}</p>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<p class="status-small-text">商品の状態</p>
					</div>
					<div class="col-sm-6">
						<p class="status-small">${item.statusName}</p>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<p class="seller-small-text">出品者</p>
					</div>

					<div class="col-sm-6">
						<p class="seller-name">${user.name}</p>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<p class="star1-reference">★</p>
					</div>

					<div class="col-sm-3">
						<p class="star2-reference">★</p>
					</div>

					<div class="col-sm-3">
						<p class="star3-reference">★</p>
					</div>

					<div class="col-sm-3">
						<p class="reference-evaluation-count">
							<a href="Evaluation">(110)</a>
						</p>
					</div>
				</div>

				<hr class=hr_reference2>


				<img class="comment-image1" src="https://placehold.jp/80x80.png"
					alt="カードの画像">

				<div class="balloon-left">
					<p>${comment.text}</p>
				</div>

				<p class="comment-name1">${user.name}</p>


				<img class="comment-image2" src="https://placehold.jp/80x80.png"
					alt="カードの画像">

				<div class="balloon-right">
					<p>${comment.text}</p>
				</div>

				<p class="comment-name2">${user.name}</p>

				<form action="item_reference.html" method="post">
					<textarea rows="1" cols="35" class="reference-textarea"></textarea>

					<input type="submit" class="reference-comment-button btn-success"
						value="コメントする" id="send">

				</form>

			</div>
		</div>
	</div>


	<nav
		class="navbar-reference navbar-expand-sm fixed-bottom navbar-dark bg-dark">
		<p class="bottom-nav-value-text">${item.itemName}円</p>
		<a href="buy_confirm.html" target="_blank">
			<button type="button" class="reference-buy-button btn-primary">購入する</button>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navmenu1" aria-controls="navmenu1"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	</nav>
</body>
</html>